package com.example.javadlatesterow;

import java.util.Scanner;

public class TaskCalculator {

        public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Wpisz pierwszą liczbę: ");
            int numberA = scanner.nextInt();
            System.out.println("Wpisz drugą liczbę: ");
            int numberB = scanner.nextInt();

            int addition = numberA + numberB;
            System.out.println("Wynik dodawania to: " + addition);

            int subraction = numberA - numberB;
            System.out.println("Wynik odejmowania to: " + subraction);

            int muliplication = numberA * numberB;
            System.out.println("Wynik mnożenia to: " + muliplication);

            int division = numberA / numberB;
            System.out.println("Wynik dzielenia to: " + division);
    }
}
